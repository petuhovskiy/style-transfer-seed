resource "yandex_vpc_network" "network-1" {
  name = "hw7network"
}

resource "yandex_vpc_subnet" "subnet-public" {
  name           = "subnet1"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.10.0/24"]
}

resource "yandex_vpc_route_table" "nat-rt" {
  network_id = yandex_vpc_network.network-1.id
}

resource "yandex_vpc_subnet" "subnet-private" {
  name           = "subnet2"
  zone           = "ru-central1-a"
  network_id     = yandex_vpc_network.network-1.id
  v4_cidr_blocks = ["192.168.0.0/24"]

  route_table_id = yandex_vpc_route_table.nat-rt.id
}
