
resource "yandex_message_queue" "tasks_queue" {
  name                        = "ymq-stransfer"
  visibility_timeout_seconds  = 600
  receive_wait_time_seconds   = 20
  message_retention_seconds   = 1209600
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
}

resource "yandex_function_trigger" "my_trigger" {
  name        = "ququ-to-st"
  description = "any description"
  message_queue {
      queue_id = yandex_message_queue.tasks_queue.arn
      service_account_id = yandex_iam_service_account.sa.id
      batch_cutoff = 1
      batch_size = 1
  }
  function {
    id = yandex_function.style-transfer-function.id
    service_account_id = yandex_iam_service_account.sa.id
  }
}
