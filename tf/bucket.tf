resource "yandex_storage_bucket" "input-bucket" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = "stransfer-src"

  cors_rule {
    allowed_headers = ["*"]
    allowed_methods = ["PUT", "POST"]
    allowed_origins = ["*", "http://localhost"]
    expose_headers  = ["ETag"]
    max_age_seconds = 100
  }
}

resource "yandex_storage_bucket" "output-bucket" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = "stransfer-res"
}

resource "yandex_storage_bucket" "static-bucket" {
  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
  bucket = "stransfer-static"

  website {
      index_document = "index.html"
  }
}

resource "yandex_storage_object" "html-client-2" {
  bucket = "stransfer-static"
  key    = "index.html"
  source = "index.html"

  access_key = yandex_iam_service_account_static_access_key.sa-static-key.access_key
  secret_key = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
}

resource "yandex_function_trigger" "bucket_trigger" {
  name        = "bucket-to-ququ"
  description = "any description"

  object_storage {
      bucket_id = yandex_storage_bucket.input-bucket.id
      create = true
  }

  function {
    id = yandex_function.validate-input-function.id
    service_account_id = yandex_iam_service_account.sa.id
  }
}
