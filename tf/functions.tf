data "archive_file" "hello_zip" {
    type        = "zip"
    source_dir  = "../functions/hello"
    output_path = "hello.zip"
}

data "archive_file" "validate_zip" {
    type        = "zip"
    source_dir  = "../functions/validate"
    output_path = "validate.zip"
}

data "archive_file" "style_transfer_zip" {
    type        = "zip"
    source_dir  = "../functions/style_transfer"
    output_path = "style_transfer.zip"
}

data "archive_file" "create_task_zip" {
    type        = "zip"
    source_dir  = "../functions/create_task"
    output_path = "create_task.zip"
}

data "archive_file" "list_tasks_zip" {
    type        = "zip"
    source_dir  = "../functions/list_tasks"
    output_path = "list_tasks.zip"
}

data "archive_file" "get_task_zip" {
    type        = "zip"
    source_dir  = "../functions/get_task"
    output_path = "get_task.zip"
}

resource "yandex_function" "validate-input-function" {
  name               = "validate-input"
  description        = "any description"
  user_hash          = "v21"
  runtime            = "python37"
  entrypoint         = "index.handler"
  memory             = "512"
  execution_timeout  = "3"
  service_account_id = yandex_iam_service_account.sa.id
  content {
    zip_filename = data.archive_file.validate_zip.output_path
  }
  environment = {
      AWS_ACCESS_KEY_ID = yandex_iam_service_account_static_access_key.sa-static-key.access_key
      AWS_SECRET_ACCESS_KEY = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
      YDB_ENDPOINT = "grpcs://ydb.serverless.yandexcloud.net:2135"
      YDB_DATABASE = "/ru-central1/b1g1p9g7tjtdscj38jen/etn03n0ot2654cvfoktm"
      USE_METADATA_CREDENTIALS = "1"
  }
}

resource "yandex_function" "style-transfer-function" {
  name               = "style-transfer"
  description        = "any description"
  user_hash          = "v37"
  runtime            = "python37"
  entrypoint         = "index.handler"
  memory             = "512"
  execution_timeout  = "3"
  service_account_id = yandex_iam_service_account.sa.id
  content {
    zip_filename = data.archive_file.style_transfer_zip.output_path
  }
  environment = {
      AWS_ACCESS_KEY_ID = yandex_iam_service_account_static_access_key.sa-static-key.access_key
      AWS_SECRET_ACCESS_KEY = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
      YDB_ENDPOINT = "grpcs://ydb.serverless.yandexcloud.net:2135"
      YDB_DATABASE = "/ru-central1/b1g1p9g7tjtdscj38jen/etn03n0ot2654cvfoktm"
      USE_METADATA_CREDENTIALS = "1"
  }
}

resource "yandex_function" "create-task-function" {
  name               = "create-task"
  description        = "any description"
  user_hash          = "v16"
  runtime            = "python37"
  entrypoint         = "index.handler"
  memory             = "128"
  execution_timeout  = "3"
  service_account_id = yandex_iam_service_account.sa.id
  content {
    zip_filename = data.archive_file.create_task_zip.output_path
  }
  environment = {
      AWS_ACCESS_KEY_ID = yandex_iam_service_account_static_access_key.sa-static-key.access_key
      AWS_SECRET_ACCESS_KEY = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
      YDB_ENDPOINT = "grpcs://ydb.serverless.yandexcloud.net:2135"
      YDB_DATABASE = "/ru-central1/b1g1p9g7tjtdscj38jen/etn03n0ot2654cvfoktm"
      USE_METADATA_CREDENTIALS = "1"
  }
}

resource "yandex_function" "list-tasks-function" {
  name               = "list-tasks"
  description        = "any description"
  user_hash          = "v1"
  runtime            = "python37"
  entrypoint         = "index.handler"
  memory             = "128"
  execution_timeout  = "3"
  service_account_id = yandex_iam_service_account.sa.id
  content {
    zip_filename = data.archive_file.list_tasks_zip.output_path
  }
  environment = {
      AWS_ACCESS_KEY_ID = yandex_iam_service_account_static_access_key.sa-static-key.access_key
      AWS_SECRET_ACCESS_KEY = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
      YDB_ENDPOINT = "grpcs://ydb.serverless.yandexcloud.net:2135"
      YDB_DATABASE = "/ru-central1/b1g1p9g7tjtdscj38jen/etn03n0ot2654cvfoktm"
      USE_METADATA_CREDENTIALS = "1"
  }
}

resource "yandex_function" "get-task-function" {
  name               = "get-task"
  description        = "any description"
  user_hash          = "v14"
  runtime            = "python37"
  entrypoint         = "index.handler"
  memory             = "128"
  execution_timeout  = "3"
  service_account_id = yandex_iam_service_account.sa.id
  content {
    zip_filename = data.archive_file.get_task_zip.output_path
  }
  environment = {
      AWS_ACCESS_KEY_ID = yandex_iam_service_account_static_access_key.sa-static-key.access_key
      AWS_SECRET_ACCESS_KEY = yandex_iam_service_account_static_access_key.sa-static-key.secret_key
      YDB_ENDPOINT = "grpcs://ydb.serverless.yandexcloud.net:2135"
      YDB_DATABASE = "/ru-central1/b1g1p9g7tjtdscj38jen/etn03n0ot2654cvfoktm"
      USE_METADATA_CREDENTIALS = "1"
  }
}