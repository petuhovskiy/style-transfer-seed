#!/bin/bash
# tf.sh

pushd tf

source secrets.env && terraform "$@"

popd
