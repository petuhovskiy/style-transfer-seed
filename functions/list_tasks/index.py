import argparse
import time
import json
import os
import boto3
import uuid
from botocore.client import Config

import datetime
import typing
from kikimr.public.sdk.python import client as ydb
from concurrent.futures import TimeoutError

_ydb_endpoint = os.getenv('YDB_ENDPOINT')
_ydb_database = os.getenv('YDB_DATABASE')
_ydb_path = os.getenv('YDB_PATH', '')

def create_driver():
    global session
    global driver

    print(_ydb_endpoint)
    print(_ydb_database)

    driver_config = ydb.DriverConfig(
        _ydb_endpoint, _ydb_database,
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    print(driver_config)
    driver = ydb.Driver(driver_config)
    try:
        driver.wait(timeout=15)
        print('i guess connection is ok?')
        print(driver.discovery_debug_details())
    except TimeoutError:
        print("Connect failed to YDB")
        print("Last reported errors by discovery:")
        print(driver.discovery_debug_details())
        os.exit(1)
    session = driver.table_client.session().create()

create_driver()

def insert_log(log_str):
    query = """
    DECLARE $logId AS Utf8;
    DECLARE $content AS Utf8;
    UPSERT INTO logs (log_id, content) VALUES ($logId, $content)
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$logId': str(uuid.uuid4()) + "-list-tasks",
            '$content': log_str,
        },
        commit_tx=True
    )

def create_task(task):
    query = """
    DECLARE $taskId AS Utf8;
    DECLARE $status AS Utf8;
    DECLARE $style AS Utf8;
    DECLARE $srcBucketId AS Utf8;
    DECLARE $objectId AS Utf8;
    DECLARE $parentTaskId AS Utf8;
    UPSERT INTO tasks (task_id, status, style, src_bucket_id, object_id, parent_task_id)
    VALUES ($taskId, $status, $style, $srcBucketId, $objectId, $parentTaskId);
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$taskId': task['task_id'],
            '$status': task['status'],
            '$style': task['style'],
            '$srcBucketId': task['src_bucket_id'],
            '$objectId': task['object_id'],
            '$parentTaskId': task['parent_task_id'],
        },
        commit_tx=True
    )

def update_task_status(event, status):
    database_id = event['database_id']

    query = """
    DECLARE $taskId AS Utf8;
    DECLARE $status AS Utf8;
    UPDATE tasks SET status = $status WHERE task_id = $taskId;
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$taskId': database_id,
            '$status': status,
        },
        commit_tx=True
    )

botosession = boto3.session.Session()
s3 = botosession.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net',
    config=Config(signature_version="s3v4")
)

input_bucket = "stransfer-src"
output_bucket = "stransfer-res"

def list_all_tasks():
    query = """
    SELECT * FROM tasks;
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {},
        commit_tx=True
    )

    return result_sets[0].rows

# list all tasks
def handler(event, context):
    insert_log(json.dumps(event))

    # style = event['queryStringParameters']['style']

    # object_id = str(uuid.uuid4())

    # task = {
    #     'task_id': str(uuid.uuid4()),
    #     'status': 'NEW',
    #     'style': style,
    #     'src_bucket_id': input_bucket,
    #     'object_id': object_id,
    #     'parent_task_id': '',
    # }

    # create_task(task)

    # presigned_url = s3.generate_presigned_url(
    #     "put_object",
    #     Params={"Bucket": task['src_bucket_id'], "Key": task['object_id']},
    #     ExpiresIn=10000,
    # )

    # resp = {
    #     'task': task,
    #     'upload_link': presigned_url
    # }

    resp = list_all_tasks()

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': resp
    }
