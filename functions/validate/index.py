import argparse
import time
import json
import os
import boto3
import uuid

import cv2 as cv
import numpy as np

import datetime
import typing
from kikimr.public.sdk.python import client as ydb
from concurrent.futures import TimeoutError

_ydb_endpoint = os.getenv('YDB_ENDPOINT')
_ydb_database = os.getenv('YDB_DATABASE')
_ydb_path = os.getenv('YDB_PATH', '')

def create_driver():
    global session
    global driver

    print(_ydb_endpoint)
    print(_ydb_database)

    driver_config = ydb.DriverConfig(
        _ydb_endpoint, _ydb_database,
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    print(driver_config)
    driver = ydb.Driver(driver_config)
    try:
        driver.wait(timeout=15)
        print('i guess connection is ok?')
        print(driver.discovery_debug_details())
    except TimeoutError:
        print("Connect failed to YDB")
        print("Last reported errors by discovery:")
        print(driver.discovery_debug_details())
        os.exit(1)
    session = driver.table_client.session().create()

create_driver()

def create_tables(path=''):
    session.create_table(
        os.path.join(path, 'logs'),
        ydb.TableDescription()
        .with_column(ydb.Column('log_id', ydb.OptionalType(ydb.PrimitiveType.Utf8)))
        .with_column(ydb.Column('content', ydb.OptionalType(ydb.PrimitiveType.Utf8)))
        .with_primary_key('log_id')
    )

create_tables(_ydb_database)

def insert_log(log_str):
    query = """
    DECLARE $logId AS Utf8;
    DECLARE $content AS Utf8;
    UPSERT INTO logs (log_id, content) VALUES ($logId, $content)
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$logId': str(uuid.uuid4()) + "-validate-func",
            '$content': log_str,
        },
        commit_tx=True
    )

def update_task_status(event, status):
    database_id = event['database_id']

    query = """
    DECLARE $taskId AS Utf8;
    DECLARE $status AS Utf8;
    UPDATE tasks SET status = $status WHERE task_id = $taskId;
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$taskId': database_id,
            '$status': status,
        },
        commit_tx=True
    )

def get_tasks(bucket_id, object_id):
    query = """
    DECLARE $srcBucketId AS Utf8;
    DECLARE $objectId AS Utf8;
    SELECT * FROM tasks WHERE src_bucket_id = $srcBucketId AND object_id = $objectId;
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$srcBucketId': bucket_id,
            '$objectId': object_id,
        },
        commit_tx=True
    )

    return result_sets[0].rows

client = boto3.client(
    service_name='sqs',
    endpoint_url='https://message-queue.api.cloud.yandex.net',
    region_name='ru-central1'
)

botosession = boto3.session.Session()
s3 = botosession.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)

queue_url = client.get_queue_url(QueueName='ymq-stransfer').get('QueueUrl')
print('Created queue url is "{}"'.format(queue_url))

def handler(event, context):

    insert_log(json.dumps(event))

    mystr = ""
    for item, value in os.environ.items():
        mystr += '{}: {}\n'.format(item, value)

    mystr += 'Created queue url is "{}"\n'.format(queue_url)

    for msg_info in event['messages']:
        process_event(msg_info)

    
    # print('Successfully sent test message to queue')

    return {
        'statusCode': 200,
        'body': 'Hello World!\n' + mystr,
    }

# Source for this function:
# https://github.com/jrosebr1/imutils/blob/4635e73e75965c6fef09347bead510f81142cf2e/imutils/convenience.py#L65
def resize_img(img, width=None, height=None, inter=cv.INTER_AREA):
    dim = None
    h, w = img.shape[:2]

    if width is None and height is None:
        return img
    elif width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    elif height is None:
        r = width / float(w)
        dim = (width, int(h * r))

    resized = cv.resize(img, dim, interpolation=inter)
    return resized

def process_event(msg_info):
    print(msg_info)
    details = msg_info['details']
    print(details)
    bucket_id = details['bucket_id']
    print(bucket_id)
    object_id = details['object_id']
    print(object_id)

    valid = False
    try:
        get_object_response = s3.get_object(Bucket=bucket_id,Key=object_id)
        image_data = get_object_response['Body'].read()

        img = cv.imdecode(np.asarray(bytearray(image_data)), cv.IMREAD_COLOR)
        img = resize_img(img, width=600)

        valid = True
    except Exception as e:
        print(e)
        print(object_id + " is invalid")

    db_tasks = get_tasks(bucket_id, object_id)

    for task in db_tasks:
        print('processing ' + str(task) + " " + str(msg_info))

        my_message = {
            'bucket_id': bucket_id,
            'object_id': object_id,
            'model_name': task.style,
            'database_id': task.task_id
        }

        status = 'PROCESSING'
        if not valid:
            status = 'INVALID'

        update_task_status(my_message, status)

        if valid:
            client.send_message(
                QueueUrl=queue_url,
                MessageBody=json.dumps(my_message)
            )