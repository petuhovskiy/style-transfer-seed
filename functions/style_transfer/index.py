import argparse
import time
import json
import os
import boto3
import uuid
import requests
import urllib.request 

import cv2 as cv
import numpy as np

import datetime
import typing
from kikimr.public.sdk.python import client as ydb
from concurrent.futures import TimeoutError

botosession = boto3.session.Session()
s3 = botosession.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net'
)

input_bucket = "stransfer-src"
output_bucket = "stransfer-res"

model_prefix = '/tmp/'
models = ['the_scream.t7', 'mosaic.t7', 'feathers.t7']

def predict(net, img, h, w):
    blob = cv.dnn.blobFromImage(img, 1.0, (w, h),
                                (103.939, 116.779, 123.680), swapRB=False, crop=False)

    print('[INFO] Setting the input to the model')
    net.setInput(blob)

    print('[INFO] Starting Inference!')
    start = time.time()
    out = net.forward()
    end = time.time()
    print('[INFO] Inference Completed successfully!')

    # Reshape the output tensor and add back in the mean subtraction, and
    # then swap the channel ordering
    out = out.reshape((3, out.shape[2], out.shape[3]))
    out[0] += 103.939
    out[1] += 116.779
    out[2] += 123.680
    out /= 255.0
    out = out.transpose(1, 2, 0)

    # Printing the inference time
    print('[INFO] The model ran in {:.4f} seconds'.format(end - start))

    return out


# Source for this function:
# https://github.com/jrosebr1/imutils/blob/4635e73e75965c6fef09347bead510f81142cf2e/imutils/convenience.py#L65
def resize_img(img, width=None, height=None, inter=cv.INTER_AREA):
    dim = None
    h, w = img.shape[:2]

    if width is None and height is None:
        return img
    elif width is None:
        r = height / float(h)
        dim = (int(w * r), height)
    elif height is None:
        r = width / float(w)
        dim = (width, int(h * r))

    resized = cv.resize(img, dim, interpolation=inter)
    return resized


def process_image_sol(image_data, model):
    net = cv.dnn.readNetFromTorch(model)
    # img = cv.imread(image)
    img = cv.imdecode(np.asarray(bytearray(image_data)), cv.IMREAD_COLOR)
    img = resize_img(img, width=600)
    h, w = img.shape[:2]
    out = predict(net, img, h, w)
    out = cv.convertScaleAbs(out, alpha=255.0)

    image_string = cv.imencode('.jpg', out)[1].tostring()
    return image_string
    # s3.put_object(Bucket="surveillance-cam", Key = imageName, Body=image_string)
    # cv.imwrite(output, out)


_ydb_endpoint = os.getenv('YDB_ENDPOINT')
_ydb_database = os.getenv('YDB_DATABASE')
_ydb_path = os.getenv('YDB_PATH', '')

def create_driver():
    global session
    global driver

    print(_ydb_endpoint)
    print(_ydb_database)

    driver_config = ydb.DriverConfig(
        _ydb_endpoint, _ydb_database,
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    print(driver_config)
    driver = ydb.Driver(driver_config)
    try:
        driver.wait(timeout=15)
        print('i guess connection is ok?')
        print(driver.discovery_debug_details())
    except TimeoutError:
        print("Connect failed to YDB")
        print("Last reported errors by discovery:")
        print(driver.discovery_debug_details())
        os.exit(1)
    session = driver.table_client.session().create()

create_driver()


def create_tables(path=''):
    session.create_table(
        os.path.join(path, 'logs'),
        ydb.TableDescription()
        .with_column(ydb.Column('log_id', ydb.OptionalType(ydb.PrimitiveType.Utf8)))
        .with_column(ydb.Column('content', ydb.OptionalType(ydb.PrimitiveType.Utf8)))
        .with_primary_key('log_id')
    )

create_tables(_ydb_database)

def insert_log(log_str):
    query = """
    DECLARE $logId AS Utf8;
    DECLARE $content AS Utf8;
    UPSERT INTO logs (log_id, content) VALUES ($logId, $content)
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$logId': str(uuid.uuid4())+'-style-transfer',
            '$content': log_str,
        },
        commit_tx=True
    )

def update_task_status(event, status, out_bucket_id, out_object_id):
    database_id = event['database_id']

    query = """
    DECLARE $taskId AS Utf8;
    DECLARE $status AS Utf8;
    DECLARE $outBucketId AS Utf8;
    DECLARE $outObjectId AS Utf8;
    UPDATE tasks SET status = $status, dst_bucket_id = $outBucketId, dst_object_id = $outObjectId WHERE task_id = $taskId;
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$taskId': database_id,
            '$status': status,
            '$outBucketId': out_bucket_id,
            '$outObjectId': out_object_id,
        },
        commit_tx=True
    )

def get_task_by_id(task_id):
    query = """
    DECLARE $taskId AS Utf8;
    SELECT * FROM tasks WHERE task_id = $taskId;
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$taskId': task_id,
        },
        commit_tx=True
    )

    return result_sets[0].rows[0]

def delete_if_done(task):
    print('delete if done ' + str(task))

    if task.delete_on_finish != 1:
        print('delete is not required')
        return

    print('deleting')
    s3.delete_object(Bucket=task.src_bucket_id, Key=task.object_id)

def recalc_parent_tasks(event):
    database_id = event['database_id']

    task = get_task_by_id(database_id)

    parent_id = task.parent_task_id
    if not parent_id:
        print('task ' + database_id + ' has no parents')
        delete_if_done(task)
        return

    parent = get_task_by_id(parent_id)
    print(str(parent))



    query = """
    DECLARE $taskId AS Utf8;
    SELECT * FROM tasks WHERE parent_task_id = $taskId;
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$taskId': parent_id,
        },
        commit_tx=True
    )

    child_tasks = result_sets[0].rows
    print(str(child_tasks))

    all_done = True
    for child in child_tasks:
        if child.status != 'DONE':
            return

    update_task_status({'database_id': parent_id}, 'DONE', '', '')
    delete_if_done(parent)

def handler(event, context):

    insert_log(json.dumps(event))

    for msg_info in event['messages']:
        print(msg_info)
        body = msg_info['details']['message']['body']
        print('msg body: ' + body)
        event = json.loads(body)

        process_image(event)

    return {}

def process_image(event):
    bucket_id = event['bucket_id']
    object_id = event['object_id']
    model_name = event['model_name']

    if models.count(model_name) <= 0:
        print('skipping event because unknown model: ' + str(event))
        return

    get_object_response = s3.get_object(Bucket=bucket_id,Key=object_id)
    image_data = get_object_response['Body'].read()

    image_string = process_image_sol(image_data, model_prefix+model_name)

    out_id = str(uuid.uuid4())
    s3.put_object(Bucket=output_bucket, Key=out_id, Body=image_string)

    update_task_status(event, 'DONE', output_bucket, out_id)
    recalc_parent_tasks(event)

def download_model(model_name):
    res_file = model_prefix + model_name
    
    url = 'https://gitlab.com/sol/style-transfer-seed/-/raw/master/models/{}'.format(model_name)
    print('downloading {} to {}'.format(url, res_file))

    # r = requests.get(url, allow_redirects=True)
    # open(res_file, 'wb').write(r.content)
    
    urllib.request.urlretrieve(url, res_file)

    # testfile = urllib.URLopener()
    # testfile.retrieve(url, res_file)

for mdl in models:
    download_model(mdl)

