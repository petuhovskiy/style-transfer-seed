import argparse
import time
import json
import os
import boto3
import uuid
from botocore.client import Config

import datetime
import typing
from kikimr.public.sdk.python import client as ydb
from concurrent.futures import TimeoutError

_ydb_endpoint = os.getenv('YDB_ENDPOINT')
_ydb_database = os.getenv('YDB_DATABASE')
_ydb_path = os.getenv('YDB_PATH', '')

def create_driver():
    global session
    global driver

    print(_ydb_endpoint)
    print(_ydb_database)

    driver_config = ydb.DriverConfig(
        _ydb_endpoint, _ydb_database,
        credentials=ydb.construct_credentials_from_environ(),
        root_certificates=ydb.load_ydb_root_certificate(),
    )
    print(driver_config)
    driver = ydb.Driver(driver_config)
    try:
        driver.wait(timeout=15)
        print('i guess connection is ok?')
        print(driver.discovery_debug_details())
    except TimeoutError:
        print("Connect failed to YDB")
        print("Last reported errors by discovery:")
        print(driver.discovery_debug_details())
        os.exit(1)
    session = driver.table_client.session().create()

create_driver()

def insert_log(log_str):
    query = """
    DECLARE $logId AS Utf8;
    DECLARE $content AS Utf8;
    UPSERT INTO logs (log_id, content) VALUES ($logId, $content)
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$logId': str(uuid.uuid4()) + "-get-task",
            '$content': log_str,
        },
        commit_tx=True
    )

def create_task(task):
    query = """
    DECLARE $taskId AS Utf8;
    DECLARE $status AS Utf8;
    DECLARE $style AS Utf8;
    DECLARE $srcBucketId AS Utf8;
    DECLARE $objectId AS Utf8;
    DECLARE $parentTaskId AS Utf8;
    UPSERT INTO tasks (task_id, status, style, src_bucket_id, object_id, parent_task_id)
    VALUES ($taskId, $status, $style, $srcBucketId, $objectId, $parentTaskId);
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$taskId': task['task_id'],
            '$status': task['status'],
            '$style': task['style'],
            '$srcBucketId': task['src_bucket_id'],
            '$objectId': task['object_id'],
            '$parentTaskId': task['parent_task_id'],
        },
        commit_tx=True
    )

def update_task_status(event, status):
    database_id = event['database_id']

    query = """
    DECLARE $taskId AS Utf8;
    DECLARE $status AS Utf8;
    UPDATE tasks SET status = $status WHERE task_id = $taskId;
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$taskId': database_id,
            '$status': status,
        },
        commit_tx=True
    )

botosession = boto3.session.Session()
s3 = botosession.client(
    service_name='s3',
    endpoint_url='https://storage.yandexcloud.net',
    config=Config(signature_version="s3v4")
)

input_bucket = "stransfer-src"
output_bucket = "stransfer-res"

def list_all_tasks():
    query = """
    SELECT * FROM tasks;
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {},
        commit_tx=True
    )

    return result_sets[0].rows

def get_task_by_id(task_id):
    query = """
    DECLARE $taskId AS Utf8;
    SELECT * FROM tasks WHERE task_id = $taskId;
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$taskId': task_id,
        },
        commit_tx=True
    )

    return result_sets[0].rows[0]

def get_all_links(task, dlink):
    res = []
    if dlink:
        res.append(dlink)

    query = """
    DECLARE $taskId AS Utf8;
    SELECT * FROM tasks WHERE parent_task_id = $taskId;
    """

    prepared_query = session.prepare(query)
    result_sets = session.transaction().execute(
        prepared_query, {
            '$taskId': task.task_id,
        },
        commit_tx=True
    )

    child_tasks = result_sets[0].rows
    print('child tasks: ' + str(child_tasks))

    for child in child_tasks:
        if child.status == 'DONE' and child.dst_object_id:
            res.append(s3.generate_presigned_url(
                "get_object",
                Params={"Bucket": child.dst_bucket_id, "Key": child.dst_object_id},
                ExpiresIn=10000,
            ))

    return res

# list all tasks
def handler(event, context):
    insert_log(json.dumps(event))

    task_id = event['queryStringParameters']['task_id']

    task = get_task_by_id(task_id)
    download_link = ''

    if task.status == 'DONE' and task.dst_object_id:
        download_link = s3.generate_presigned_url(
            "get_object",
            Params={"Bucket": task.dst_bucket_id, "Key": task.dst_object_id},
            ExpiresIn=10000,
        )

    return {
        'statusCode': 200,
        'headers': {
            'Content-Type': 'application/json'
        },
        'body': {
            'task': task,
            'download_link': download_link,
            'all_links': get_all_links(task, download_link)
        }
    }
